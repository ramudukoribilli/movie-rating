from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import permission_required
from .forms  import Movieform,Ratingform,NewUserForm
from django.contrib import messages
# Create your views here.
def home(request):
    query = request.GET.get("title")
    allMovies = None
    if query:
        allMovies = Movie.objects.filter(name__icontains=query)
    else:
        allMovies = Movie.objects.all()  # select * from movie
    
    context = {
        "movies": allMovies,
    }

    return render(request, 'movierating/index.html', context)

def detail(request,id):
    movie = Movie.objects.get(pk = id)
    comment = Message.objects.filter(movie = movie)
    rating = Rating.objects.filter(user = request.user, movie = movie)
    if len(rating) == 0:
        rating = None
    context = {
        "movie": movie,
        "comments" : comment,
        "ratings" : rating
    }
    return render(request, 'movierating/details.html', context)

def login_view(request):
    if request.method == 'GET':
        return render(request, 'movierating/login.html')
    if request.method == 'POST':
        username = request.POST.get('username', "")
        password = request.POST.get('password', "")
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request,user)
            return redirect('movierating:home')
        else:
            return render(request, 'movierating/login.html', {'message': 'Invalid credentials, try again'})
def logout_view(request):
    logout(request)
    return redirect('movierating:login')

def add_movie(request):
    submitted=False
    if request.method == "POST":
        form = Movieform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('movierating:home')
    else:
        form = Movieform
    if 'submitted' in request.GET:
        submitted=True
    return render(request, 'movierating/addmovie.html', {'form':form})

def add_comment(request):
    if request.method == 'POST':
        comment = request.POST.get('comment')
        movie = Movie.objects.get(pk = request.POST.get('movie'))
        user = request.user
        new_comment = Message(comment = comment, user = user, movie = movie)

        new_comment.save()
        return redirect('movierating:detail', id = request.POST.get('movie'))


def add_rating(request):
    if request.method == 'POST':
        rating = int(request.POST.get('rating'))
        movie = Movie.objects.get(pk = request.POST.get('movie'))
        user = request.user
        new_rating, created = Rating.objects.get_or_create(user = user, movie = movie)
        new_rating.rating = rating
        new_rating.save()
        allratings = Rating.objects.filter(movie = movie)
        totalrating = sum([item.rating for item in allratings])
        avg_rating = totalrating / len(allratings)

        movie.averageRating = avg_rating
        movie.save()
        return redirect('movierating:detail', id = request.POST.get('movie'))



def resigter(request):
     return render(request, 'movierating/resigter.html',)

def add_rate(request):
    submitted=False
    if request.method == "POST":
        form = Ratingform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('movierating:home')
    else:
        form = Ratingform
    if 'submitted' in request.GET:
        submitted=True
    return render(request, 'movierating/addrating.html', {'form':form})



def register_request(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("movierating:login")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm()
	return render (request=request, template_name="movierating/register.html", context={"register_form":form})
