from django.urls import path
from . import views

app_name='movierating'

urlpatterns=[
     path('home', views.home, name="home"),
     path('details/<int:id>', views.detail, name="detail"),
     path('login', views.login_view, name='login'),
     path('logout', views.logout_view, name='logout'),
     path('addmovie', views.add_movie,name='addmovie'),
     path('add-comment', views.add_comment, name='add-comment'),
     path('add-rating',views.add_rating,name='add-rating'),
     path('addrating', views.add_rate,name='addrating'),
      path("register", views.register_request, name="register")
]