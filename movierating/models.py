from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Movie(models.Model):
    name = models.CharField(max_length=300)
    director = models.CharField(max_length=300)
    cast = models.CharField(max_length=800)
    description = models.TextField(max_length=5000)
    release_date = models.DateField()
    averageRating = models.FloatField(default=0)
    image = models.URLField(default=None, null=True)
    videos = models.URLField(default=None, null=True)
    def __str__(self):
        return f'{self.name}'

class Message(models.Model):
    comment=models.CharField(max_length=600)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user=models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.comment

class Rating(models.Model):
    rating = models.IntegerField(null=True)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user', 'movie')

        def __str__(self):
            return f'{self.rating}'
