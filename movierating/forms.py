from django import forms
from django.forms import ModelForm
from .models import Movie,Rating
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class Movieform(ModelForm):
    class Meta:
        model=Movie
        fields=('name', 'director', 'cast','description','release_date','averageRating','videos','image')

        widgets={
            'name':forms.TextInput({'class':'form-control','placeholder': 'Movie name'}),
            'director':forms.TextInput({'class':'form-control','placeholder': 'Director name'}),
            'cast':forms.TextInput({'class':'form-control','placeholder': 'the cast in the movie'}),
            'description':forms.Textarea({'class':'form-control','placeholder':'enter letters lessthan 5000'}),
            'averageRating':forms.TextInput({'class':'form-control','placeholder':'Rate a movie below 10'}),
            'videos':forms.TextInput({'class':'form-control','placeholder': 'please keep the link of youtube'}),
            'image':forms.TextInput({'class':'form-control','placeholder': 'please keep the link of image'}),
            'release_date':forms.TextInput({'class':'form-control','placeholder': 'the date in the format:YYY/MM/DD eg:2023-01-19'}),
        }

class Ratingform(ModelForm):
    class Meta:
        model=Rating
        fields=('rating','movie','user')

        widgets = {
            'rating':forms.TextInput({'class': 'form-control'}),
            'movie':forms.Select({'class': 'form-control'}),
            'user':forms.Select({'class': 'form-control'})
        }

class NewUserForm(UserCreationForm):
	email = forms.EmailField(required=True)
	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")
	def save(self, commit=True):
		user = super(NewUserForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user